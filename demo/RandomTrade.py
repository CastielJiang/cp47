import zmq
import threading
import random
from pymql import MtraderNamePipeAPI
from time import sleep
from time import time



class RandomTrade:
    def __init__(self,api=None,random_function=None, trade_interval=900):
        if api==None:
            self.api=MtraderNamePipeAPI.MtraderNamePipeAPI()
        else:
            self.api = api
        if random_function==None:
            self.random_function = lambda: random.choice([True, False])
        else:
            self.random=random_function
        self.trade_interval=trade_interval
        self.last_trade_time = time()
    def start(self):
        self.threads = threading.Thread(target=self._on_tick, daemon=True)
        self.threads.start()
    def trade(self, tick):
        if time() >= (self.last_trade_time + self.trade_interval):
            self.last_trade_time = time()
            isBuy = self.random_function()
            if isBuy:
                print('Buy')
                res = self.api.construct_and_send(action="TRADE", actionType="ORDER_TYPE_BUY", symbol="EURUSD", volume=0.1, takeprofit=1.2, price=tick['data']['bid'])
                print(res)
            else:
                print('Sell')
                res = self.api.construct_and_send(action="TRADE", actionType="ORDER_TYPE_SELL", symbol="EURUSD", volume=0.1)
                print(res)
    def _on_tick(self):
        socket = self.api.tick_socket()
        while True:
            try:
                last_tick = socket.recv_json()
            except zmq.ZMQError:
                raise zmq.NotDone("Live data ERROR")
            print("bid", last_tick["data"]["bid"], "ask:", last_tick["data"]["ask"])
            self.trade(last_tick)

if __name__ == "__main__":
    rt = RandomTrade()
    rt.start()
    while True:
        sleep(10)





