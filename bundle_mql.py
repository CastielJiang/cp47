# bundle/merge all mql file to one
from os.path import join

output_file_name = "backend.mq5"

output_contents = ""
json_mqh = ""
np_client = ""
start = "\n\n\n //----- HEADER file merged (START) ------\n\n\n"
end = "\n\n\n //----- HEADER file merged (END) ------\n\n\n"

def auto_path(p):
    """
    convert unix path to pathform dependent path
    """
    path_list = p.split("/")
    return join(*path_list)

JSON_NAMED_PIPE_FILE_APTH = auto_path("dependency/MQL5-JSON-API/Experts/JsonAPINamePipe.mq5")
JSON_FILE_PATH = auto_path("dependency/MQL5-JSON-API/Include/json.mqh")
NAMEPIPE_CLIENT_FILE_PATH = auto_path("dependency/MQL5-JSON-API/Include/NamePipeClient.mqh")
with open(JSON_NAMED_PIPE_FILE_APTH, "r") as f:
    output_contents = f.read()

with open(JSON_FILE_PATH, "r", encoding="utf8") as f:
    json_mqh = f.read()

with open(NAMEPIPE_CLIENT_FILE_PATH, "r") as f:
    np_client = f.read()


output_contents = output_contents.replace("#include <Json.mqh>", start + json_mqh + end)
output_contents = output_contents.replace("#include <NamePipeClient.mqh>", start + np_client + end)
with open(output_file_name, "w", encoding="utf8") as f:
    f.write(output_contents)
