from pymql.LiveTradeExecutor import LiveTradeExecutor
c = iter([1,1,0,0, None, None, 1,1,0,0])
class AI:
    def predict(self, data):
        """
        Parameter:
        data example:
        {
            "<DATE>": "2019.08.23",
            "<TIME>": 00:40:00,
            "<OPEN>": 1.10805,
            "<HIGH>": 1.10808,
            "<LOW>" : 1.10789,
            "<CLOSE>" :1.10806
        }
        """
        # to access data
        time = data["<TIME>"]
        high_price = data["<HIGH>"]
        # returns -1, 0, or 1
        return next(c)

ai = AI()
# to backtest, use the following code
# excutor = BackTester(=ai)
# to run live, use the following code
executor = LiveTradeExecutor(ai)
executor.run()

