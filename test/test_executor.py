import unittest
import example
from datetime import datetime
from pymql import LiveTradeExecutor
# import install
# import bundle_mql
history1 = [
    [int(datetime(2019,10,5,1,2,3).timestamp()),
    1.10810,
    1.10812,
    1.10788,
    1.10809,
    1],

    [int(datetime(2019,10,5,1,3,3).timestamp()),
    1.10805,
    1.10808,
    1.10789,
    1.10806,
    1],

    [int(datetime(2019,10,5,1,4,3).timestamp()),
    1.10800,
    1.10810,
    1.10780,
    1.10805,
    1]
]
class MockApi:
    def __init__(self):
        self.no_op_called = False
        self.last_cas_args = None
        self.last_cas_kwargs = None
    def get_next_candle(self):
        return \
        {
            "data": history1[-1],
            "historyData": history1,
            "symbol": "USDJPY"
        }
    def no_op(self):
        self.no_op_called = True
    def construct_and_send(self, *args, **kwargs):
        self.last_cas_args = args
        self.last_cas_kwargs = kwargs


class MockAI:
    def __init__(self, predict_results):
        self.symbol = None
        self.data = None
        self.history = None
        self.predict_results_iter = iter(predict_results)
    def predict(self, symbol, data, history):
        self.symbol = symbol
        self.data = data
        self.history = history
        return next(self.predict_results_iter)

class TestExecutor(unittest.TestCase):
    def test_live_executor_symbol_parse(self):
        ai = MockAI([{"action": "skip"}])
        l = LiveTradeExecutor.LiveTradeExecutor(ai, transport=MockApi())
        l.run_once()
        self.assertEqual(ai.symbol, "USDJPY")

    def test_live_executor_data_parse(self):
        ai = MockAI([{"action": "skip"}])
        l = LiveTradeExecutor.LiveTradeExecutor(ai, transport=MockApi())
        l.run_once()
        data = {
            "<DATE>": "2019.10.05",
            "<TIME>": "01:04:03",
            "<OPEN>": 1.10800,
            "<HIGH>": 1.10810,
            "<LOW>" : 1.10780,
            "<CLOSE>" : 1.10805,
        }
        self.assertEqual(ai.data, data)

    def test_live_executor_history_parse(self):
        ai = MockAI([{"action": "skip"}])
        l = LiveTradeExecutor.LiveTradeExecutor(ai, transport=MockApi())
        l.run_once()
        self.assertEqual(ai.history, history1)

    def test_live_executor_skip(self):
        ai = MockAI([{"action": "skip"}])
        api = MockApi()
        l = LiveTradeExecutor.LiveTradeExecutor(ai, transport=api)
        l.run_once()
        self.assertEqual(api.no_op_called, True)

    def test_live_executor_sell(self):
        ai = MockAI([{"action": "sell"}])
        api = MockApi()
        l = LiveTradeExecutor.LiveTradeExecutor(ai, transport=api)
        l.run_once()
        self.assertEqual(api.last_cas_kwargs, {"action": "TRADE", "actionType": "ORDER_TYPE_SELL"})

    def test_live_executor_buy(self):
        ai = MockAI([{"action": "buy"}])
        api = MockApi()
        l = LiveTradeExecutor.LiveTradeExecutor(ai, transport=api)
        l.run_once()
        self.assertEqual(api.last_cas_kwargs, {"action": "TRADE", "actionType": "ORDER_TYPE_BUY"})