from pymql.AbstractTradeExecutor import AbstractTradeExecutor
from time import time, localtime, strftime

# TODO: improve consistency of naming; candle for candle
def candle_to_dict(data_raw):
    data_timestamp = localtime(data_raw[0])
    data = {
        "<DATE>": strftime("%Y.%m.%d", data_timestamp),
        "<TIME>": strftime("%H:%M:%S", data_timestamp),
        "<OPEN>": data_raw[1],
        "<HIGH>": data_raw[2],
        "<LOW>": data_raw[3],
        "<CLOSE>": data_raw[4]
    }
    return data


class LiveTradeExecutor(AbstractTradeExecutor):
    """
    Class for live trading
    arguments:
        trading_algrithm: a any object with a method name `predict`
                    the `predict` method will be called on every update
    """
    def __init__(self, trading_algrithm, transport=None):
        self.trading_algrithm = trading_algrithm
        if transport is None:
            from pymql.MtraderNamePipeAPI import MTraderNamePipeAPI
            self.api = MTraderNamePipeAPI()
        else:
            self.api = transport
    def run_once(self):
        candle = self.api.get_next_candle()
        data_raw = candle["data"]
        data = candle_to_dict(data_raw)
        history = candle["historyData"]
        symbol = candle["symbol"]
        predict_result = self.trading_algrithm.predict(symbol,data, history)
        if type(predict_result) is not dict:
            raise ValueError("Value must return a dictionary type")
        trade_action = predict_result['action']
        predict_result.pop("action")
        extra_args = predict_result
        if trade_action == "skip":
            self.api.no_op()
        elif trade_action.lower() == "buy":
            res = self.api.construct_and_send(
                action="TRADE",
                actionType="ORDER_TYPE_BUY",
                **extra_args)
        elif trade_action.lower() == "sell":
            res = self.api.construct_and_send(
                action="TRADE",
                actionType="ORDER_TYPE_SELL",
                **extra_args)
        else:
            raise ValueError("action must be 'skip', 'buy' or 'sell'")


    def run(self):
        while True:
            self.run_once
            