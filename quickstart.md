# 1. Installation (skip if already installed)
## 1.1 MQL (backend) install
1. open pymql_release_v{version}\MQL5_install and copy the content of backend.mq5
2. open `MetaEditor` click new and select expert adviser(template), click next, fill in the name `Experts\backend` then click next until finish. replace content of file with content copied in step 1 
3. click compile.
## 1.2 Python API (client) install
1.install `pywin32`

*for Anaconda Python, run `conda install -c anaconda pywin32 `*

*for Python official release run `python -m pip install pywin32`*

# 2 Write Python program
find example.py and modify the body of the `predict` method

# 3 Running the program

## backtest
1. Start python program (running `python example.py`)
2. Open `MetaTrader 5`
3. Under `Strategy Tester`, find `expert` field and choose `backend.ex5` then
choose the desired date, symbol, timeframe etc.

## Live:
1. Start python program (running `python example.py`)
2. Open `MetaTrader 5`
3. right click the desired chart then select the desired `timeframe`
4. Under `Navigator` under experts, drag backend to the chart
5. profit

*the python program must be started before mql5; restarting python requires restarting MQL5 backend(JsonAPINamePipe expert)*


