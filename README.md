** Project : Building a Forex Trading bot**

As a world-class platform for trading stock, analyzing financial markets, MetaTrader is well-recognized for its comprehensive functions and timely sharing of information.
MetaTrader use Expert Advisors and MQL to mainipulate data in this platform. Nevertheless, this editor uses language that is so difficult to be easily comprehended even for algorithm developers.
The world needs a basic – and easily extensible – high performance bridge between external (non-MQL) programming languages and MetaTrader 5.
Currently there is no single useful interface to allow traders to fully explore algorithms in non-MQL languages. 
Given the popularity and usefulness of python in data processing, the meaning of this interface between MQL and python is momumental.

*You can [watch our video](https://www.youtube.com/watch?v=SZFHVgr2x5E) for a full demo of MetaTrader 4 in this tutorial.
Currently the world is progressing towards MetaTrader 5, but no such needy and functional interface is handy to use.*

---

## Purpose of the project

Target user: algorithm developer, solo trader.


1.pull data


2.connect to python to visualize data


3.send requests to mainipulate MQL for stock operations


4.back test algorithms on historical data to predict the perfomance of different trading strategies


5.automate the above process to trade automatically on live data，and the API should support operation on multiple charts

### Before you move on, go ahead and explore the repository. We have attached all the minutes and status reports on wiki.

|Status Report|Client Meeting|Tutor feedback|Group Meeting|

Link is here:https://bitbucket.org/CastielJiang/cp47/wiki/Home

## Include files and set up the environment

The setup is not so easy due to the limited compatibility and UI of MetaTrader in non-windows system., but we manage to list a step-by-step guide .

Check if Metatrader 5 and windows system are installed. 

Proceed to install the API according to the [use manual here](https://docs.google.com/document/d/1u6sEy2yjZQHEbm4SuiiJoeVFxFYfYQVtD6_wXxob-90/edit?usp=sharing).

---
---
## Demo the connection and manipulate MQL for automatic buy/sell functions.

1.Please refer to the steps in 3.1 of the user manual to demo the API for algorithms back testing (downloading and tesign on the historial data would be automatic).

2.Please refer to the steps in 3.2 of the user manual to demo the API for using the algorighm to live trade.

3.Please refer to the steps in 4 of the user manual to demo the API for testing and trading of multiple currencies.

---
Now that you're more familiar with our API, go ahead and add a new file locally. You can use it to interface with MQL using python scripts.