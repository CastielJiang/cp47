from shutil import copyfile, move
import os.path
from os.path import isfile, isdir, join, exists
import os
import sys
from time import strftime

mql_project_path = r'C:\Users\cai\AppData\Roaming\MetaQuotes\Terminal\D0E8209F77C8CF37AD8BF550E51FF075\MQL5'

installation_files = {
    "Include": [
        r"dependency\MQL5-JSON-API\Include"
    ],
    "Libraries": [
    ],
    "Experts": [
        r"dependency\MQL5-JSON-API\Experts"
    ]
}

def copy_files_with_backup(path_from, path_to):
    """
        copy all file/dir inside path_from to path_to
        also creates backup file inplace
    """
    entries = os.listdir(path_from)
    for ent in entries:
        real_from_path = join(path_from, ent)
        new_path = join(path_to, ent)

        if isdir(real_from_path):
            if not exists(new_path):
                os.mkdir(new_path)
            copy_files_with_backup(real_from_path, new_path)
        elif isfile(real_from_path):
            if isfile(new_path):
                bk_substr = "_backup_" + strftime("%Y_%m_%d_%H_%M_%S") 
                bk_path = join(path_to, ent + bk_substr)
                move(new_path, bk_path)
            copyfile(real_from_path, new_path)
        else:
            print("skipping", real_from_path, "not a file nor dir")



if __name__ == "__main__":
    if not os.path.isdir(mql_project_path):
        print("error: mql project path not found")
        sys.exit(1)
    for d, entries in installation_files.items():
        cp_to = os.path.join(mql_project_path, d)
        for ent in entries:
            copy_files_with_backup(ent, cp_to)