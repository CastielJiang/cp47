"""
This is the example file for pymql;
`executor` will call the `predict` function on every new candle recieved for each currencies in the symbol list

The number of data sent by the MQL backend can be configure
by changing the line
    `input int nCandleToPushConfig=10000;`

The symbols/currencies send by the MQL backend can be configured by
changing the line
    string symbolList[] = {"USDJPY","GBPUSD", "EURUSD"};

Argument given to the `predict` function:

symbol: symbol/currency name (given as string)
data : latest candle recieved (see example below)
{
    "<DATE>": "2019.08.23",
    "<TIME>": 00:40:00,
    "<OPEN>": 1.10805,
    "<HIGH>": 1.10808,
    "<LOW>" : 1.10789,
    "<CLOSE>" :1.10806
}
history: is an list where
each element is a candle encoded as an list
candle_n is the latest candle

[candle_0, candle_1, .., candle_n ]
candle_1[0] = (long) rate.time;
candle_1[1] = (double) rate.open;
candle_1[2] = (double) rate.high;
candle_1[3] = (double) rate.low;
candle_1[4] = (double) rate.close;
candle_1[5] = (double) rate.tick_volume;

candle_n-2 is contains the same data as the `data` argument
to convert history to pasdas dataframe:
use the following line of code
history_dataframe = pd.DataFrame(history, columns=("time", "open", "high", "low", "close", "tick_volume"))
"""

# optional import, only use for demo
from datetime import datetime
#using pip install panda if missing modules pandas
import pandas as pd
# non-optional import:
from pymql.LiveTradeExecutor import LiveTradeExecutor
class AI:
    def predict(self, symbol, data, history):
        # convert history to pandas dataframe 
        history_dataframe = pd.DataFrame(history, columns=("time", "open", "high", "low", "close", "tick_volume"))
        print("symbol/ currency", symbol)
        print(history_dataframe.describe())
        ######################################################################################################
        # EXAMPLE FOR DETECTING SYMBOL
        # 
        # 
        # if(symbol == "EURUSD"):
        #   PredictFunctionForEURUSD(data,history)
        # elif(symbol == "GBPUSD"):
        #   PredictFunctionForGBPUSD(data,history)
        # elif ....
        # ......
        #
        ######################################################################################################
        # to access data
        time = data["<TIME>"]
        high_price = data["<HIGH>"]
        print("-----")
        print("current time: ", time, "high: ", high_price)
        print("size of history:", len(history))
        print("oldest candle date:", datetime.fromtimestamp(history[0][0]).strftime("%x %X"))
        print("oldest data", history[0])
        print("newest candle date:", datetime.fromtimestamp(history[-1][0]).strftime("%x %X"))
        print("-----")
        # skip example:
        # return {"action":"skip"}
        # buy example:
        # return {"action":"buy", "symbol": symbol, "takeprofit": 0 , "stoploss": 0, "volume": 0.4}
        # sell example:
        # return {"action":"sell", "symbol": symbol, "takeprofit": 0 , "stoploss": 0, "volume": 0.4}
        # Predict algorithm examples:
        # If the currency rate of last candle is greater than the current currency ratio, make a sell action
        # else if they are same, take no action.
        # else make a buy action

        # NOTICE: for sell action, the takeprofit must lower than the current currency ratio and the stoploss 
        # must higher than the current currency ratio. As we are predict the ratio will go down, we can only take 
        # profit if the currency ratio drop.
        # For buy action, it's the opposite of sell action.
        
        ############### Currently, the API does not support track order and make close action #######################
        if(data["<CLOSE>"] > history[-3][4]):

            return {"action":"sell", 
                    "symbol": symbol, 
                    "takeprofit": data["<CLOSE>"] - 0.1, 
                    "stoploss": data["<CLOSE>"] + 0.1, 
                    "volume": 0.4}

        elif(data["<CLOSE>"] == history[-3][4]):

            return {"action":"skip"}
        else:
            return {"action":"buy", 
                    "symbol": symbol, 
                    "takeprofit": data["<CLOSE>"] + 0.1, 
                    "stoploss": data["<CLOSE>"] - 0.1, 
                    "volume": 0.4}
if __name__ == "__main__":
    ai = AI()
    executor = LiveTradeExecutor(ai)
    executor.run()

