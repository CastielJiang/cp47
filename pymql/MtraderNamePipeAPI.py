import threading
from queue import Queue, Empty
import json
from time import sleep
import win32pipe, win32file, pywintypes
import struct

RECIEVE_MAX_BUFFER_SIZE = 1024 * 10



class NamePipeServer:
    """direction == "sync_send" to send message
       direction == "receive" to receive message
       sync_send sends data and wait for "OK" reply 
    """
    def __init__(self, name, direction, heartbeat_interval=1000):
        self.threads = []
        self.queue = Queue(100)
        self.name = name
        self.heartbeat_interval = heartbeat_interval
        if direction == "sync_send":
            self.target_func = self._sync_send_loop
        elif direction == "receive":
            self.target_func = self._receive_loop
        else:
            raise ValueError("direction must be sync_send or received")
        self.pipe = win32pipe.CreateNamedPipe(
        r'\\.\pipe\MQL5.JSONAPI.' + name,
        win32pipe.PIPE_ACCESS_DUPLEX,
        win32pipe.PIPE_TYPE_BYTE | win32pipe.PIPE_READMODE_BYTE | win32pipe.PIPE_WAIT,
        win32pipe.PIPE_UNLIMITED_INSTANCES,256*1024,256*1024,1000,
        None)
        self.connect_thread = threading.Thread(
            target=self._connect_worker,
        )
        self.connect_thread.start()
        self.worker_stop = False

    def stop_worker(self):
        if self.child_thread:
            self.worker_stop = True
            self.child_thread.join()
            self.child_thread = None

    def _connect_worker(self):
        win32pipe.ConnectNamedPipe(self.pipe, None)
        print(f"{self.name} pipe connected")
        self.child_thread = threading.Thread(
            target=self.target_func, 
            args=tuple()
        )
        self.child_thread.start()
    def send_json(self, dict_object):
        """ take a dictionary and send as json string"""
        # print("putting data into queue")
        self.queue.put(json.dumps(dict_object))
    def _recieve_str(self):
        mlen, strsize_raw = win32file.ReadFile(self.pipe, 4)
        strsize = struct.unpack("<i", strsize_raw)[0]
        mlen, bytes_recieved = win32file.ReadFile(self.pipe, strsize)
        return bytes_recieved.decode()
    def _receive_loop(self):
        # print("recieve thread started")
        while not self.worker_stop:
           # print(self.name, "receiving")
            str_received = self._recieve_str()
            self.queue.put(str_received)
            # print('size {} Received {}'.format(strsize, str_received))

    def _sync_send_loop(self):
        while not self.worker_stop:
            # print("waiting for data to send")
            try:
                msg = self.queue.get(block=True, timeout=self.heartbeat_interval)
                #print("got msg from queue")
            except Empty:
                msg = ""
                # print("heartbeat")
            # print("sending")
            to_send = self._str_to_byte_with_length_prefix(msg)
            win32file.WriteFile(self.pipe, to_send)
            # heartbeat msg does not response OK
            if msg == "":
                s = self._recieve_str()
                if s != "OK":
                    raise ValueError("Send failed", "got", s, "instead of", "OK")
    def recv_json(self):
        msg = self.queue.get()
        return json.loads(msg)
    def send_heartbeat(self):
        self.queue.put("{}")
    @staticmethod
    def _str_to_byte_with_length_prefix(s:str):
        return len(s).to_bytes(4, "little", signed=True) + s.encode()
        
class MTraderNamePipeAPI:
    def __init__(self):
        # connect to server sockets
        self.sys_server = NamePipeServer("SYS", "sync_send")
        self.data_server = NamePipeServer("DATA","receive")
        self.live_server = NamePipeServer("LIVE","receive")
        self.events_server = NamePipeServer("STREAM","receive")
        self.tick_server = NamePipeServer("TICK","receive")
    def _send_request(self, data: dict) -> None:
        """Send request to server via ZeroMQ System socket"""
        self.sys_server.send_json(data)
    def _pull_reply(self):
        """Get reply from server via Data socket with timeout"""
        msg = self.data_server.recv_json()
        return msg

    def get_next_candle(self):
        """ 
        get next rate/candle object from MQL
        this function will block until a cancle is recieved
        """
        return self.live_server.recv_json()
    def construct_and_send(self, **kwargs) -> dict:
        """Construct a request dictionary from default and send it to server"""

        # default dictionary
        request = {
            "action": None,
            "actionType": None,
            "symbol": None,
            "chartTF": None,
            "fromDate": None,
            "toDate": None,
            "id": None,
            "magic": None,
            "volume": None,
            "price": None,
            "stoploss": None,
            "takeprofit": None,
            "expiration": None,
            "deviation": None,
            "comment": None
        }

        # update dict values if exist
        for key, value in kwargs.items():
            if key in request:
                request[key] = value
            else:
                raise KeyError('Unknown key in **kwargs ERROR')

        # send dict to server
        self._send_request(request)

        # return server reply
        return self._pull_reply()
    def no_op(self):
        """no operation (sends do nothing to MQL)""" 
        self.sys_server.send_heartbeat()

