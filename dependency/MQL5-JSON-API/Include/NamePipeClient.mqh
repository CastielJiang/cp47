#property strict
#include <Files\FilePipe.mqh>

class NamePipeClient{
    string namePipePath;
    CFilePipe  pipe;
    public:
    NamePipeClient(string name)
    {
        namePipePath = "\\\\.\\pipe\\MQL5.JSONAPI." + name;
    }
    virtual ~NamePipeClient(){
        // pipe.close();
    }
    // for connect and reconnecting
    bool connect(int waitfor=1){
       bool r = pipe.Open(namePipePath,FILE_READ|FILE_WRITE|FILE_BIN) != INVALID_HANDLE;
       if (r){
         Print("Connected to ", namePipePath);
       }else{
           Print("Failed to connect to", namePipePath);
       }
       return r;
    }
    bool disconnect() {
        Print(namePipePath, "disconnected");
        pipe.Close();
        return true;
    }
    bool send(const string& msg){
        return pipe.WriteString(msg) > 0;
    }
    bool recieve(string & msg){
        bool r = pipe.ReadString(msg);
        if(r){
         // return false if heartbeat message
         return StringLen(msg) > 0;
        }else{
        // failed to recieve
         return false;
        }
    }
};

