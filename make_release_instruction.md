# How to make a release file (zip)

1. create a folder `pymql_release_v{version number}`
2. create `MQL5_install` under 1 
3. copy `dependency\MQL5-JSON-API\Experts\JsonAPINamePipe.mq5` as `backend.mq5` replace the two `#include <>` statement with the file it includes
dependency\MQL5-JSON-API\Include\json.mqh
dependency\MQL5-JSON-API\Include\TCPClient.mqh

4. copy pymql as-is to 1
5. copy quickstart.md and run 
`python -m markdown quickstart.md > quickstart.html` to generate html quickstart
6. zip 1
7. put under release
8. tag the version on git