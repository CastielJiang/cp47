import unittest
import example
import io
import sys
from datetime import datetime
# import install
# import bundle_mql
class testExample(unittest.TestCase):
    data = {
        "<DATE>": "2019.11.5",
        "<TIME>": "00:40:00",
        "<OPEN>": 1.10805,
        "<HIGH>": 1.10808,
        "<LOW>" : 1.10789,
        "<CLOSE>" :1.10806
    }
    data2 = {
        "<DATE>": "2019.11.5",
        "<TIME>": "00:25:00",
        "<OPEN>": 999999,
        "<HIGH>": 999999,
        "<LOW>" : 999999,
        "<CLOSE>" :999999
    }
    data3 = {
        "<DATE>": "2019.11.5",
        "<TIME>": "00:55:00",
        "<OPEN>": 1.00001,
        "<HIGH>": 1.00001,
        "<LOW>" : 1.00001,
        "<CLOSE>" :1.00001
    }
    history = [
        [int(datetime(2019,11,5,0,25,0).timestamp()),
        1.10810,
        1.10812,
        1.10788,
        1.10809,
        1],

        [int(datetime(2019,11,5,0,40,0).timestamp()),
        1.10805,
        1.10808,
        1.10789,
        1.10806,
        1],

        [int(datetime(2019,11,5,0,55,0).timestamp()),
        1.10800,
        1.10810,
        1.10780,
        1.10805,
        1]
    ]
    history2 = [
        [int(datetime(2019,11,5,0,25,0).timestamp()),
        1.10810,
        1.10812,
        1.10788,
        1.10800,
        1],

        [int(datetime(2019,11,5,0,40,0).timestamp()),
        1.10805,
        1.10808,
        1.10789,
        1.10806,
        1],

        [int(datetime(2019,11,5,0,55,0).timestamp()),
        1.10800,
        1.10810,
        1.10780,
        1.10805,
        1]
    ]
    history3 = [
        [int(datetime(2019,11,5,0,25,0).timestamp()),
        1.10805,
        1.10808,
        1.10789,
        1.10806,
        1],

        [int(datetime(2019,11,5,0,40,0).timestamp()),
        1.10805,
        1.10808,
        1.10789,
        1.10806,
        1],

        [int(datetime(2019,11,5,0,55,0).timestamp()),
        1.10800,
        1.10810,
        1.10780,
        1.10805,
        1]
    ]
    def testOperationForBuy(self):
        AI = example.AI()
        output = AI.predict("EURUSD",self.data,self.history)
        self.assertEqual(output,{'action': 'buy', 'symbol': 'EURUSD', 'takeprofit': 1.2080600000000001, 'stoploss': 1.00806, 'volume': 0.4})
    def testOperationForSell(self):
        AI = example.AI()
        output2 = AI.predict("EURUSD",self.data,self.history2)
        self.assertEqual(output2,{'action': 'sell', 'symbol': 'EURUSD', 'takeprofit': 1.00806, 'stoploss': 1.2080600000000001, 'volume': 0.4})
    def testOperationForSkip(self):
        AI = example.AI()
        output3 = AI.predict("EURUSD",self.data,self.history3)
        self.assertEqual(output3,{'action': 'skip'})
    def testOutputForData(self):
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput            
        AI = example.AI()
        output = AI.predict("EURUSD",self.data,self.history)
        sys.stdout = sys.__stdout__
        print(capturedOutput.getvalue())
        self.assertEqual(capturedOutput.getvalue().split()[-6:-1],["newest", "candle", "date:", "11/05/19", "00:55:00"])

test = testExample()
test.testOperationForBuy()
test.testOperationForSell()
test.testOperationForSkip()
test.testOutputForData()