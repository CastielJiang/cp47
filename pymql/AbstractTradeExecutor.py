from abc import ABC, abstractmethod

class AbstractTradeExecutor(ABC):
    """
    abstract class that defindes
    what a TradeExecutor should have.
    >> What is a TradeExecutor?
    A TradeExecutor sits between an trading algrithm 
    and the lower MTradeAPI (MtraderNamePipe & MTraderAPI)    
    >> What does a TradeExecutor do? 
    The TraderExecutor recieves instruction from the trading_algrithm
    in the form of -1, 0, 1 and translates these into actual
    pymql api call
    """
    @abstractmethod
    def run(self):
        """
        start an infinite loop and that sends
        data to the trading_algrithm through
        trading_algrithm.predict method
        """
        pass